package com.example.fredrick.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    private EditText mm_field;
    private EditText inches_field;
    private double inches;
    private Button convert;
    private Button exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convert=(Button)findViewById(R.id.button);
        exit=(Button)findViewById(R.id.button2);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inches_field=findViewById(R.id.editText2);
                mm_field=findViewById(R.id.editText);
                double inputLength=0;
                if (mm_field.getText().toString().length()==0){
                    mm_field.setError("Input valid length");
                }
                else {
                    inputLength=Double.parseDouble(mm_field.getText().toString());
                    double answer=convertToInches(inputLength);
                    inches_field.setText(String.valueOf(answer));

                }
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    //formula of conversion to inches
    public double convertToInches(double milimeters){
        inches=milimeters/25.4;
        return inches;
    }


}
